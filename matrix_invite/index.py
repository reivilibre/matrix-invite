from flask import Blueprint, render_template, current_app

blueprint = Blueprint("index", __name__)


@blueprint.route('/')
def index():
    return render_template('index_home.html', config=current_app.config['display'])


@blueprint.route('/credits')
def attribution():  # credits is a Python built-in.
    return render_template('index_credits.html', config=current_app.config['display'])


@blueprint.route('/get_connected')
def get_connected():
    return render_template('index_getconnected.html', config=current_app.config['display'],
                           homeserver_url=f"https://{ current_app.config['homeserver']['host'] }")
