import re
from os import path

INVITATION_KEY_REGEX = r"^\s*([a-zA-Z]+)\s*([0-9]+)\s*([a-zA-Z]+)\s*$"


def transform_key(key_text):
    match = re.match(INVITATION_KEY_REGEX, key_text)

    if match is None:
        raise ValueError(
            "That doesn't look like an invitation key.")

    return f"{match.group(1)}{match.group(2)}{match.group(3)}".lower()


def get_db_path(app_config, config_path):
    return path.join(path.dirname(config_path), app_config['database']['path'])
