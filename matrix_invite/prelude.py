import sys


def eprint(*all):
    print(*all, file=sys.stderr)


class UserShowableError(Exception):
    pass
