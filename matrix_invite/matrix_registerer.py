# Copyright 2015, 2016 OpenMarket Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import hmac, hashlib, json, ssl
from urllib import request

from .prelude import *


def request_registration(user, password, server_location, shared_secret, admin=False):
    """
    Matrix User Registration Code, Taken from the Synapse scripts and adapted for Python3
    """
    nonce_req = request.Request(
        f"{server_location}/_synapse/admin/v1/register",
        headers={'Content-Type': 'application/json'}
    )

    try:
        fnonce = request.urlopen(nonce_req, context=ssl.SSLContext(ssl.PROTOCOL_TLSv1_2))
        nonce_json = json.loads(fnonce.read().decode('utf-8'))
        if 'nonce' not in nonce_json:
            raise UserShowableError('No nonce in response.')
        nonce = nonce_json['nonce']
        fnonce.close()

        mac = hmac.new(
            key=shared_secret.encode(),
            digestmod=hashlib.sha1,
        )

        # THIS is only valid for the v1 endpoint
        mac.update(nonce.encode())
        mac.update(b"\x00")
        mac.update(user.encode())
        mac.update(b"\x00")
        mac.update(password.encode())
        mac.update(b"\x00")
        mac.update(b"admin" if admin else b"notadmin")

        mac = mac.hexdigest()

        data = {
            "nonce": nonce,
            "username": user,
            "password": password,
            "mac": mac,
            "admin": admin,
        }

        server_location = server_location.rstrip("/")

        req = request.Request(
            f"{server_location}/_synapse/admin/v1/register",
            data=json.dumps(data).encode(),
            headers={'Content-Type': 'application/json'}
        )

        eprint(json.dumps(data).encode())

        f = request.urlopen(req, context=ssl.SSLContext(ssl.PROTOCOL_SSLv23))
        f.read()
        f.close()
        return True
    except request.HTTPError as e:
        # eprint(dir(e.info()))
        eprint(f"ERROR! Received {e.code} {e.reason} whilst registering {user}")
        if 400 <= e.code < 500:
            if e.info().get_content_type() == "application/json":
                resp = json.load(e)
                if "error" in resp:
                    eprint(resp["error"])
                    raise UserShowableError(resp["error"], True)
        return False
    except Exception as exc:
        eprint(f"Other exception whilst registering {user}")
        eprint(exc)
        return False
