import os
import secrets
import traceback
from datetime import datetime

from flask import Blueprint, render_template, request, current_app
from matrix_client.client import MatrixClient
from wtforms import Form, StringField, validators, PasswordField, ValidationError

from matrix_invite import matrix_registerer
from matrix_invite.common import transform_key, INVITATION_KEY_REGEX
from matrix_invite.prelude import *

blueprint = Blueprint("registration", __name__)

reg_config = current_app.config['registration']


def valid_key_validator(_form, field):
    if len(field.errors) == 0:
        conn = current_app.connection
        cursor = conn.cursor()
        timestamp = datetime.utcnow().timestamp()

        key = transform_key(field.data)
        print("tk", key)

        cursor.execute('SELECT 1 FROM invitation_keys WHERE key = ? AND expiry_timestamp > ? '
                       ' AND revocation_timestamp > ?', (key, timestamp, timestamp,))
        result = cursor.fetchone()

        if result is None:
            raise ValidationError(
                "That invitation key does not exist. Perhaps it expired, or maybe it never existed at all."
                " Check its spelling.")


class RegistrationForm(Form):
    invite_key = StringField('Invitation Key', [
        validators.regexp(INVITATION_KEY_REGEX,
                          message="The invitation key doesn't look correct. It should be 2 words and a number."),
        valid_key_validator])
    username = StringField('Username', [
        validators.length(min=reg_config['username_len_min'],
                          max=reg_config['username_len_max']),
        validators.regexp(reg_config['username_allowed_regex'],
                          message=reg_config['username_allowed_description'])])
    password = PasswordField('Password', [
        validators.length(min=8, max=255),
        validators.equal_to('password_again',
                            message='The passwords do not match.')])
    password_again = PasswordField('Password (again)')


del reg_config  # we don't need this anymore -- clean it up


def process_registation(form):
    conn = current_app.connection
    timestamp = datetime.utcnow().timestamp()
    conn.execute('BEGIN IMMEDIATE TRANSACTION')
    try:
        key = transform_key(form.invite_key.data)
        c = conn.cursor()
        c.execute('SELECT key, single_use, creator FROM invitation_keys WHERE key = ? AND expiry_timestamp > ? '
                  ' AND revocation_timestamp > ?', (key, timestamp, timestamp,))
        key_row = c.fetchone()

        if key_row is None:
            raise UserShowableError("The invitation key vanished during the registration, "
                                    "so the registration could not be completed.")

        cacfg = current_app.config

        if not matrix_registerer.request_registration(form.username.data, form.password.data,
                                                      cacfg['homeserver']['url'],
                                                      cacfg['homeserver']['secret_registration_token'],
                                                      admin=False):
            raise UserShowableError('User __NOT__ registered on Matrix server.')

        if key_row['single_use']:
            c.execute('UPDATE invitation_keys SET revocation_timestamp = ? WHERE key = ?', (timestamp, key,))

        c.execute(
            'INSERT INTO created_accounts (invitation_key, registrant_name, registration_timestamp) VALUES (?, ?, ?)',
            (key, form.username.data, timestamp,))

        c.execute('COMMIT')

        botuser = f"@{cacfg['bot']['username']}:{cacfg['display']['mxid_domain']}"

        try:
            client = MatrixClient(cacfg['homeserver']['url'])
            client.login(username=form.username.data, password=form.password.data, device_id="InitialRegistration")
            client.create_room(invitees=[botuser])
        except Exception:
            traceback.print_exc()
            raise UserShowableError(f"Your account has been registered, but it wasn't possible to "
                                    f"automatically add {botuser} to "
                                    f"your contacts. Please add the bot manually.")

        return key_row['creator']
    finally:
        try:
            conn.execute('ROLLBACK TRANSACTION')
        except Exception as e:
            eprint("in finally statement: ", e)


@blueprint.route('/', methods=['GET', 'POST'])
def index():
    form = RegistrationForm(request.form)

    if request.method == 'POST' and form.validate():
        # noinspection PyBroadException
        try:
            contact_username = process_registation(form)

            return render_template('registration_success.html', config=current_app.config['display'],
                                   username=form.username.data,
                                   homeserver=current_app.config['homeserver']['url'],
                                   contact_username=contact_username)
        except UserShowableError as e:
            if len(e.args) > 1 and e.args[1]:  # allow form again
                form.username.errors.append(f"Notice from Matrix Server: {e.args[0]}")
            else:
                return render_template('registration_error.html', error=e.args[0], config=current_app.config['display'])
        except Exception:
            traceback.print_exc()
            return render_template('registration_error.html', error='Other exception (ask the server admin).',
                                   config=current_app.config['display'])

    with open(os.path.join(os.path.dirname(__file__), 'eff-and-rei-reasonablylong-english.txt')) as f:
        wordlist = [ln.strip() for ln in f if len(ln.strip()) > 0]

    def generate_passphrase(numwords=4):
        pieces = [wordlist[secrets.randbelow(len(wordlist))] for _ in range(numwords)]
        return ' '.join(pieces)

    passphrase_suggestions = [generate_passphrase() for _ in range(5)]

    return render_template('registration_index.html', config=current_app.config['display'], form=form,
                           passphrase_suggestions=passphrase_suggestions)


@blueprint.route('/invite')
def invite():
    return render_template('registration_invite.html', config=current_app.config['display'],
                           bot_username=current_app.config['bot']['username'])
