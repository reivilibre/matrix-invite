import shutil
from os import path
import sqlite3
import sys

import pytoml
from flask import Flask
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.wsgi import WSGIContainer
from matrix_invite.prelude import *
from matrix_invite import bot
from matrix_invite import common


def create_app(app_config):
    app = Flask(__name__)

    for key in app_config.keys():
        app.config[key] = app_config[key]
    # app.config['display'] = app_config['display']
    # app.config['registration'] = app_config['registration']
    app.debug = True

    conn = sqlite3.connect(common.get_db_path(app_config, CONFIG_PATH))
    app.connection = conn
    conn.row_factory = sqlite3.Row

    with app.app_context():
        from matrix_invite import index, registration

        # register submodules
        app.register_blueprint(index.blueprint, url_prefix='/')
        app.register_blueprint(registration.blueprint, url_prefix='/registration')

    return app


if len(sys.argv) < 2:
    eprint("Please specify the config file as the first parameter.\n"
           "It should be a .toml file which will be generated on first run.")
    sys.exit(1)
else:
    CONFIG_PATH = sys.argv[1]  # config file from path

if not path.isfile(CONFIG_PATH):
    eprint("No config file found; generating one.")
    shutil.copyfile('default_config.toml', CONFIG_PATH)

with open(CONFIG_PATH, 'r') as fin:
    app_config = pytoml.load(fin)

dbpath = common.get_db_path(app_config, CONFIG_PATH)

if not path.isfile(dbpath):
    eprint("No database found; copying the blank one.")
    shutil.copyfile(path.join(path.dirname(__file__), "blank_database.db"), dbpath)

app = create_app(app_config)

mybot = bot.RegServBot(app_config['homeserver']['url'], app_config['bot']['username'],
                       app_config['bot']['password'], app_config, dbpath)

# thread = threading.Thread(name="",
#                          target=lambda: mybot.start())
mybot.start()

http_server = HTTPServer(WSGIContainer(app))

try:
    port = app_config['listen']['port']
except KeyError:
    port = 8080

try:
    bind_addr = app_config['listen']['address']
except KeyError:
    bind_addr = ""

http_server.listen(port, address=bind_addr)
IOLoop.instance().start()
