import os.path
import re
import secrets
import sqlite3
from datetime import datetime

from matrix_client.client import MatrixClient

from matrix_invite.common import transform_key
from matrix_invite.prelude import *


class RegServBot:
    def __init__(self, server, username, password, config, db_path):
        self.client = MatrixClient(f"{server}")
        self.client.add_invite_listener(self.on_invited)
        self.client.add_listener(self.on_event_message, "m.room.message")
        self.client.add_listener(self.on_event_membership, "m.room.member")
        self.config = config
        self.db_path = db_path
        # Not legal in multithreading. self.connection = connection

        self.token = self.client.login(username=username, password=password, device_id="RegServBot", sync=True, limit=0)

    def start(self):
        eprint("Started RegServBot")
        eprint("sync token", self.client.sync_token)
        self.client.start_listener_thread()

    def on_invited(self, room_id, _state):
        # TODO check how many users are in the room. Only join if there's one.
        # for now, this is a feature not a bug ;-)
        self.client.join_room(room_id)
        print("joined", room_id)
        room = self.client.rooms[room_id]
        room.send_notice(f"Hi! Welcome to The {self.config['display']['title']}.")
        room.send_notice(f"If you want to invite someone to the server, I can create invitation keys.")
        room.send_notice(f"Send me the message 'help' for a list of what I can do!")

    def on_event_message(self, roomchunk):
        room_id = roomchunk['room_id']
        room = self.client.rooms[room_id]
        print("oemessage", roomchunk)

        sender_id_parts = roomchunk['sender'].split(':')
        assert sender_id_parts[0][0] == '@'
        sender_user = sender_id_parts[0][1:]
        sender_server = sender_id_parts[1]

        if sender_server != self.config['display']['mxid_domain']:
            room.send_notice("Sorry. I'm not supposed to talk to strangers. Goodbye.")
            room.leave()
            return

        if roomchunk['sender'] != f"@{self.config['bot']['username']}:{self.config['display']['mxid_domain']}" \
                and 'content' in roomchunk:
            content = roomchunk['content']
            if content['msgtype'] == 'm.text':
                text = content['body']
                eprint("Received text message: ", text)

                command_pieces = text.strip().split()
                full_command = " ".join(command_pieces)
                if re.fullmatch("help", full_command, re.I):

                    plain_text = "<Help Text (formatted message)>"
                    formatted_text = f"""<h1>{self.config['bot']['username']} Commands</h1>
<h4>Help</h4>
<p><code>help</code> — shows help.</p>
<h4>Invitations</h4>
<p><code>get invitation key</code> — gets a single-use invitation key.
<i>Currently, only single-use invitation keys can be created.</i></p>"""

                    self.client.api.send_message_event(
                        room_id,
                        "m.room.message",
                        content={
                            "msgtype": "m.text",
                            "format": "org.matrix.custom.html",
                            "body": plain_text,
                            "formatted_body": formatted_text
                        }
                    )
                elif re.fullmatch("get invitation key", full_command, re.I):
                    with open(os.path.join(os.path.dirname(__file__), 'wordlist.txt')) as f:
                        wordlist = [ln.strip() for ln in f if len(ln.strip()) > 0]

                    # different thread, different SQLite connection.
                    conn = sqlite3.connect(self.db_path)
                    c = conn.cursor()
                    c.execute('BEGIN IMMEDIATE TRANSACTION')
                    timestamp_now = datetime.utcnow().timestamp()
                    timestamp_expire = timestamp_now + (86400 * 7)

                    for iteration in range(256):
                        word1idx = secrets.randbelow(len(wordlist))
                        word2idx = secrets.randbelow(len(wordlist))
                        middle_number = secrets.randbelow(100)

                        key_human = f"{wordlist[word1idx].title()} {middle_number} {wordlist[word2idx].title()}"
                        key_db = transform_key(key_human)

                        c.execute('SELECT 1 FROM invitation_keys WHERE key = ? AND expiry_timestamp < ?',
                                  (key_db, timestamp_now,))
                        result = c.fetchone()

                        if result is None:
                            # invitation key not already in use.
                            c.execute(
                                'INSERT INTO invitation_keys '
                                '(key, creator, single_use, creation_timestamp, expiry_timestamp, revocation_timestamp)'
                                ' VALUES (?, ?, ?, ?, ?, ?)',
                                (key_db, sender_user, True, timestamp_now, timestamp_expire, timestamp_expire))
                            c.execute('COMMIT TRANSACTION')

                            plain_text = "<Invitation Key>"
                            register_url = self.config['bot']['refer_to_url']
                            formatted_text = f"""<h1>Single-Use Invitation Key</h1>
                            <h4>Invitation Key</h4>
                            <p><code>{key_human}</code> — give this to the new member.</p>
                            <h4>Validity</h4>
                            <p>This invitation key will expire in one week and can only be used once.</p>
                            <h4>Usage</h4>
                            <p>Please refer the member to <a href="{register_url}">{register_url}</a></p>"""

                            self.client.api.send_message_event(
                                room_id,
                                "m.room.message",
                                content={
                                    "msgtype": "m.text",
                                    "format": "org.matrix.custom.html",
                                    "body": plain_text,
                                    "formatted_body": formatted_text
                                }
                            )
                            break
                    else:
                        room.send_notice("Sorry. I couldn't generate a unique invite key.\n"
                                         "Maybe there are already too many?")
                        c.execute('ROLLBACK TRANSACTION')

    def on_event_membership(self, roomchunk):
        room_id = roomchunk['room_id']
        if 'membership' in roomchunk and roomchunk['membership'] == 'leave':
            # user has left the room.
            self.client.rooms[room_id].leave()
            eprint("Left room")
        print("oemembership", roomchunk)
        pass
