FROM alpine:3.7

COPY docker/setup.sh /setup.sh
COPY matrix_invite /mi_src/matrix_invite
COPY setup.py /mi_src

RUN ash /setup.sh

EXPOSE 8080

VOLUME ["/dacon"] # data & config

COPY docker/launch.sh /launch.sh

USER minvite:minvite

ENTRYPOINT ["/bin/sh", "/launch.sh"]
