# matrix-invite

## Attribution

### Word List

To obtain the word list, I ran [http://tothink.com:80/mnemonic/wordlist.html](http://web.archive.org/web/20090918202746/http://tothink.com:80/mnemonic/wordlist.html) through `tr -sc '[:alpha:]' '[\n*]'`.

Some modifications have been made to the list, for the purpose of removing American-specific spellings.

### Theming and Fonts

This work uses my [Milligramme-Noir-EnBlanc theme](https://gitlab.com/reivilibre/milligramme-noir/tree/master-enblanc) which is a fork of [Milligram by CJ Patoilo, under the MIT License](https://milligram.io/).

### Similar work

Although the work here is original, some other projects that may interest you may include:

* [matrix-registration by ZerataX](https://github.com/ZerataX/matrix-registration)

## Licence

The files in this repository are licensed under the following licence:
* The MIT Licence.

With the following exceptions:

* `matrix_invite/wordlist.txt` is from [http://tothink.com:80/mnemonic/wordlist.html](http://web.archive.org/web/20090918202746/http://tothink.com:80/mnemonic/wordlist.html).

## Installation

1) Copy the `reivilibre-font-unfading-subset.{ttf,woff,woff2}` files from [their repository](https://gitlab.com/reivilibre/reivilibre-font-unfading/tree/master/build) and insert them into the `/matrix_invite/static` directory.

2) Run `build.sh` within the `/matrix-invite-static` directory in order to emit the `entry.css` file into that static file directory.

3) Build the Docker image.

4) Deploy the Docker image, referring to the Dockerfile for information on what volumes to create and which port to put behind a reverse proxy.

Please feel free to contact me (e.g. by opening an issue) if these instructions are unclear and you are interested in running this project.

