#!/bin/sh

set -eu

versiontag=$1

docker run -d \
		--name matrix_invite \
		--mount type=volume,source=dacon_matrix_invite,destination=/dacon \
		--restart unless-stopped \
		--publish 127.0.0.1:9002:8080 \
		matrix_invite:$versiontag

