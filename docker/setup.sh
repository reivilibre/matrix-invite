#!/bin/sh

set -eu

apk add --no-cache python3

# -e to install dependencies only
pip3 install -e /mi_src

adduser -H -D -s /sbin/nologin minvite

mkdir -p /dacon

chown -R minvite:minvite /mi_src /dacon

rm /setup.sh
