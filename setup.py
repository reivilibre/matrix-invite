from setuptools import setup

REQUIRES = [
    'matrix-client>=0.3.2',
    'flask>=1.0.0',
    'tornado',
    'pytoml', 'wtforms'
]

setup(
    name='matrix_invite',
    version='0.0.2',
    packages=['matrix_invite'],
    url='https://librepush.net/matrix_invite',
    license='',
    author='reivilibre',
    author_email='',
    description='Matrix Invitation System',
    install_requires=REQUIRES
)
