#!/bin/sh

set -eu

yarn run parcel build 'style/entry/*.scss'

mkdir -p ../matrix_invite/static/
cp dist/*.css -t ../matrix_invite/static/
